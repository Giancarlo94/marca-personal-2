var express = require("express"), cors = require('cors'); //requiero modulo express
//el cors evita que un puerto desconocido se meta en mi aplicación
var app = express(); //inicializo servidor web
app.use(express.json()); //usaremos JSON 
app.use(cors()); //le permitimos a esta api usar solicitudes de la pagina 4200
app.listen(3000,() => console.log("server running on port 3000")); //usaremos puerto 3000

//base de datos ciudades
var ciudades = ["Paris","Barcelona","Barranquilla", "Montevideo","Santiago de Chile","New York"];
//filtrado para pasar a minusculas.
app.get("/ciudades",(req, res, next)=> res.json(ciudades.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase())>-1)));
//req: solicitud que lleva del servidor, res: responder al servidor


var misDestinos =[];
app.get("/my",(req, res, next) => res.json(misDestinos));//guardaremos en el servidor
app.post("/my",(req,res,next)=>{ //post pasa todos los destinos
    console.log(req.body);
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos); //recibe el json enviado por el servidor(desde Fiddler)
});

app.get("/api/translation", (req, res, next)=> res.json([ //agrego servicio para translate
    {lang: req.query.lang, key : 'HOLA', value: 'HOLA' + req.query.lang}
]));
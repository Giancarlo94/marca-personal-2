import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule,Routes } from '@angular/router'; //importo librería para trabajar con rutas
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http'; // importación para comunicar proyectos?

import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // S2Form 2 Agrego Angular forms y reactive
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store'; // redux
import { Store } from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects'; //redux

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';



import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';// al realizar el ng g c, me importa automáticamente el componente, siempre y cuando tenga éste modulo cerrado*, si no, hay que hacerlo manual, al igual que la declaración*
import { 
  DestinosViajesState,
   reducerDestinosViajes,
    initializeDestinosViajesState, 
    DestinosViajesEffects } from './models/destinos-viajes-state.model'; // redux

  import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component'; // para extensión chrome
import { AuthService } from './services/auth.service';//login
import {UsuarioLogueadoGuard} from './guards/usuario-logueado/usuario-logueado.guard';

import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component'; //login
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import {InitMyDataAction} from './models/destinos-viajes-state.model';

import Dexie from 'dexie'; //importacion para trabajar con INDEXED DB
import { DestinoViaje } from './models/destino-viaje.model';
import { identity, Observable, from, pipe } from 'rxjs';
import {flatMap} from 'rxjs/operators';

import {TranslateService, TranslateLoader, TranslateModule} from '@ngx-translate/core';

import {NgxMapboxGLModule, } from 'ngx-mapbox-gl'; //mapa
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';//Animaciones


//app config
export interface AppConfig {
  apiEndpoint : String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint : 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config'); //tecnica de inyeccion 
//fin app config


//Init Rutas hijas de vuelo 

export const childrenRoutesVuelos: Routes = [
  { path : '', redirectTo: 'main', pathMatch: 'full'},
  { path : 'main', component: VuelosMainComponent},
  { path : 'mas-info', component: VuelosMasInfoComponent},
  { path : ':id', component: VuelosDetalleComponent},
  

];


//Declaramos las rutas mediante un objeto const de tipo Routes
// Y le asignaremos un valor por defecto(Array []).. que contendrá las rutas que vamos a usar en la app
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'}, // ruta 1, Path '' indica que es la URL de mi aplicación por defecto y que me llevará a home¨*
  //patMatch, se encarga de hacer un "Match" cuando esté vacio(validar)
  { path: 'home', component: ListaDestinosComponent},// ruta 2, de Home, redirige a ListaDestinosComponent
  { path: 'destino/:id', component: DestinoDetalleComponent}, //form (id) ruta 3 desde el componente nuevo(destino, previamente ya generado con el ng generate component) a la Lista
  { path: 'login', component: LoginComponent}, //login
  
  { path: 'protected', //login
   component: ProtectedComponent,
   canActivate: [ UsuarioLogueadoGuard ]
  
  },
  { //path para rutas hijas
    path : 'vuelos',
    component: VuelosComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  }

]; // fin rutas hijas

// redux init

export interface AppState { //estado global de la app
  destinos: DestinosViajesState; 

}
const reducers: ActionReducerMap<AppState> = { //reducer globales de la app
  destinos : reducerDestinosViajes
};

const reducersInitialState = {
destinos: initializeDestinosViajesState()

};
//redux fin init

//app init, encargado de cargar todo lo que está en el servidor
export function init_app(appLoadService: AppLoadService): () => Promise<any> { // funcion que va a retornar un objeto encargado de hacer ciertas tareas cuando se inicialice la aplicación
  return () => appLoadService.initializeDestinosViajesState(); // init crea un appploadservice que se crea abajo y se invoca en esta linea
}
@Injectable()
class AppLoadService {
  constructor(private store : Store<AppState>, private http: HttpClient){}
  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders ({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body)); //InitMyDataAction está creado en el componente destinos-viajes-state,model**
  }
}
//fin app init

//inicialización Dexie dB con translate incluido
export class Translation { //objeto que guarda localmente lo que nos traemos del web service para no perder las traducciones
  constructor( public id : number, public lang: string, public key: string, public value: string){}

}

@Injectable({ // Servicio Inyectable Mydatabase que hereda de la librería Dexie, 
  providedIn: 'root'
})
export class MyDataBase extends Dexie {
  destinos : Dexie.Table<DestinoViaje, number>; //Definimos Dexie Table que representará todas las colecciones que se guardaran en nuestro Index Db
  translations: Dexie.Table<Translation, number>;
  constructor () {
    super('MyDatabase');
    this.version(1).stores({ //Defino la primer versión del objeto con los siguientes campos:
      destinos: '++id, nombre, imageUrl'
    });
    this.version(2).stores({
      destinos: '++id, nombre, imageUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDataBase();
//fin dexie dB

//init I18N
class TranslationLoader implements TranslateLoader { //cargador personalizado(custom)de traducciones
  constructor(private http: HttpClient){}
 
  getTranslation(lang: string): Observable<any>{
    const promise = db.translations
                      .where('lang') //funciones para hacer consultas en Dexie
                      .equals(lang)
                      .toArray()
                      .then(results =>{
                                      if (results.length === 0){ //Callbak (si no esta en la abse de datos hago la llamada ajax)
                                        return this.http //si no entra en la BD te retorno una promesa
                                        .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                        .toPromise()
                                        .then(apiResults => {
                                          db.translations.bulkAdd(apiResults); //Agrego a DB de traducciones local
                                          return apiResults;
                                        });
                                      }
                                      return results; //si la traduccion esta en DB retorno traduccion
                                    }).then((traducciones) => {
                                      console.log('traducciones cargadas:');
                                      console.log(traducciones);
                                      return traducciones;
                                    }).then((traducciones) => {
                                      return traducciones.map((t) =>({[t.key]: t.value})); //realizo mapeo para Ngx translate
                                    });
                  
   return from(promise).pipe(flatMap((elems) => from (elems)));  //pasamos de promesa a observable, que es con lo que funciona el framework                
  }//Flat map cambia un Array de Arrays por un único Array de traducciones
}
function HttpLoaderFactory (http: HttpClient){
  return new TranslationLoader(http);
}


// fin I18N

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,  // S2Form 1 ; Invoco 2 librerías para poder trabajar con formularios y agrego arriba
    HttpClientModule, //debe ir en este lugar 
    RouterModule.forRoot(routes), // Registro el objeto con las rutas para que las pueda  cargar el import MODULO/model-ts (?)**
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}), //vinculo nuevo modulo y debo importarlo arriba
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,//para extencion en chrome
    TranslateModule.forRoot({ //importamos el modulo de Translate para toda la aplicacion(inyector raiz)
      loader:{
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
     UsuarioLogueadoGuard, AuthService, //form
     { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE}, //provide para inyeccion token de arriba
      AppLoadService,
      { provide: APP_INITIALIZER, useFactory: init_app, deps:[AppLoadService], multi: true}, //cuando se carga la app, se buscan todas las inyecciones de dependencias hechas con APP_INITIALUZER
      MyDataBase //Agrego servicio para la inyección de dependencias    
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import {DestinosApiClient } from './../../models/destinos-api-client.model';
import {DestinoViaje} from './../../models/destino-viaje.model';
import {ActivatedRoute} from '@angular/router';
import { DestinosViajesEffects } from '../../models/destinos-viajes-state.model';
import { AppState } from '../../app.module';
import {Store} from '@ngrx/store';



//ejemplo caso 3 hasta acá, y continua con el provider
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers:[
    DestinosApiClient //dependencia principal, es regresivo de abajo hacia arriba
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino:DestinoViaje;
  style = {

    sources:{

      world:{

        type:'geojson',

        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json' //capa basica del mapa

      }

    },

    version:8,

    layers:[{

      'id': 'countries', //solo carga la capa de paises

      'type': 'fill',

      'source': 'world',

      'layout': {},

      'paint': {

        'fill-color': '#6F788A'

      }

    }]

  };

 

  constructor(private route: ActivatedRoute , private destinosApiClient: DestinosApiClient ) { }


  ngOnInit(){
   const id=this.route.snapshot.paramMap.get('id');
    //this.destino = null;
     //let  id=this.route.snapshot.paramMap.get('id'); // lineas profe... hay que estar pendiente del Api client
    this.destino=this.destinosApiClient.getById(id); //porque me dice que no tiene el método getById.
  }

}

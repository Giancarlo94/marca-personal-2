import { Component, OnInit, Input, HostBinding, Output, EventEmitter} from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { AppState } from '../../app.module';
import {Store} from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';// animaciones



@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations:[
    trigger('esFavorito', [
      state ('estadoFavorito', style({
        backgroundColor: 'Silver'
      })),
      state ('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')

    ]),
  ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input("idx") position: number;// creo variable position(indice), esta variable depende del listado,
  //por lo tanto, abría que crearla en ese componente también**
  @HostBinding('attr.class') cssClass = 'col-md-4';

  @Output() onClicked: EventEmitter<DestinoViaje>; // 1.3. Usando la DIRECTIVA para valores de salida(@output, el cual debe importarse arriba)
  // y DECLARO la propiedad CLICKED, de tipo EvenEmitter con Generic <DestinoViaje> (opcional, pero conveniente)


  constructor(private store: Store<AppState>) { 
    this.onClicked = new EventEmitter(); // 1.4. INICIALIZO y le asigno un objeto a la propiedad/evento Clicked.
    // UNA VEZ HECHO LOS 4 PASOS,  YA ESTÁ LISTA PARA USAR******

    // ESTA PROPIEDAD SE USARÁ EN EL LISTADO (Lista-destinos.component.html)****
  		
  }  		
ngOnInit() {
  }
  
ir(){ // 1. Creo el método que usamos para disparar eventos**
    this.onClicked.emit(this.destino); //1.2  Genero la acción, tal cual la quiero usar, para este caso quiero emitir
    //un evento que de a conocer, qué destino fue clicleado. HAY QUE DECLARAR ESE EVENTO ARRIBA* 'C L I K E D'
    return false; //para que no genere ningún efecto en el HTML
}

voteUp(){
  this.store.dispatch(new VoteUpAction(this.destino));
  return false;
}

voteDown(){
  this.store.dispatch(new VoteDownAction(this.destino));
  return false;
}

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service'; //login

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mensajeError: string; //login

  constructor(public authService: AuthService) { 
    this.mensajeError ='';
  }

  ngOnInit() {
  }
  
  login(username: string, password: string ): boolean {
    this.mensajeError ='';
    if (!this.authService.login(username, password)){ //login sincrónico porque estaremos chequeando con los datos constantes en el authservice
      this.mensajeError = 'login incorrecto.';
      setTimeout(function(){
      this.mensajeError = '';

    }.bind(this),2500); // en caso de que el login sea incorrecto , limpiamos el mensaje de error luego de 2.5 segundos
  }
    return false;
  }
  logout():boolean {
    this.authService.logout(); //invoca a logout. ****1. IR A USUARIO-LOGUEADO.GUARD.TS***
    return false;

  }

}

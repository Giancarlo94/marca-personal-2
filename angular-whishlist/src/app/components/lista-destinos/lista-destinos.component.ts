import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model'; // form
import { Store, State } from '@ngrx/store'; //redux
import {AppState} from '../../app.module'; //redux
import {ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model' //redux



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers:[DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>; //formulario
  updates: string []; // creo Array para
  

  constructor(private destinosApiClient: DestinosApiClient,
     private store: Store<AppState>
     ) { 
    this.onItemAdded = new EventEmitter(); //  formulario
    this.updates =[]; //inicializo updates vacío

  }

  ngOnInit(){
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => { //me suscribo a una funcion observable
      const f = data;
      if (f != null){ // if distinto de null porque el primer evento de la funcion observable está inicializado  en null
        
        this.updates.push('Se eligió:' + f.nombre); // le llevo el valor seleccionado al array**
      }
    });
    
  }

  agregado(d: DestinoViaje){ //método para guardar los datos ingresados.

  this.destinosApiClient.add(d);
  this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje){
    this.destinosApiClient.elegir(d);// Invoco método Api-client.model.ts "elegir"
    
    }

}

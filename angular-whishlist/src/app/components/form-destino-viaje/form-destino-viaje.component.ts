import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import {DestinoViaje} from '../../models/destino-viaje.model' //form
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms'; //form
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators' // librerias para autocompletador
import { ajax, AjaxResponse } from 'rxjs/ajax' //autocompletar
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>; // form declaración
  fg: FormGroup; //form variable
  minLongitud = 3; //validacion, este capitulo funciona!!
  searchResults: string[]; // autocompletado: defino array de String para usar en el autocompletado
 

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG )) private config: AppConfig) {  //inyecto dependencia fb, FormBuilder es el objeto responsable de la construccion
//inyecto app config en el cosntructor. FordwaredRef salva la situación de referencia circular
    this.onItemAdded = new EventEmitter();

    this.fg = fb.group({
      nombre: ['', Validators.compose([ //validacion
        Validators.required,//validacion
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)//validacion
      ])], // forms control para vincularlos en los tags html
      url: ['']
    });
    this.fg.valueChanges.subscribe(
      (form:any)=> { //muesta en consola los cambios en el form
      console.log('cambio el formulario: ', form);
    }
    );

    this.fg.controls['nombre'].valueChanges.subscribe(
      (value: string)=> {
        console.log('nombre cambió', value);
      }
    );

  }

  ngOnInit() {
    //detectaremos a medida que nos van escribiendo
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')  //genera observable de eventos de entrada de teclado(no solo la letra, sino toda la cadena)
      .pipe( //pipe permite hacer operaciones en serie
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string)=> ajax(this.config.apiEndpoint + '/ciudades?q='+ text)) //ajax al apiendpoint y le concatenamos ciudades
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response); //trae el documento JSON
  }

  guardar(nombre: string, url:string): boolean { //form
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;

  }

  nombreValidator(control: FormControl): { [s: string]: boolean} { //validacion
    const l= control.value.toString().trim().length;
    if (l > 0 && l<5){
      return{ invalidNombre: true};
    }
    return null;
  }
  nombreValidatorParametrizable(minLong: number): ValidatorFn {

    return(control: FormControl): { [ key: string]:boolean} | null => {
      const l= control.value.toString().trim().length;
      if (l > 0 && l<minLong){
        return{ 'minLongNombre': true};
      }
      return null;


    };
  }


}

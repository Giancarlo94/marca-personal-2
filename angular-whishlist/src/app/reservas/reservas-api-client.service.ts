import { Injectable } from '@angular/core';

@Injectable({  //Servicio INYECTABLE que se usará en otro componente(el inyectado)
  providedIn: 'root'
})
export class ReservasApiClientService {

  constructor() { }

  getAll(){
    return [{ id: 1, name: 'uno'},{id:2, name:'dos'}]; // Devuelve un Array de objetos 1 y 2
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import {ReservasApiClientService} from './reservas-api-client.service';


@NgModule({
  
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  
  providers: [ 

    //acá arriba, deberán ir las dependencias que tenga ReservasApiClientService en caso de**
    ReservasApiClientService //servicio INYECTADO, reservas.module invoca el constructor de este servicio.
  ],
  declarations: [ReservasListadoComponent, ReservasDetalleComponent]

})
export class ReservasModule { }

import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	selected: boolean; // 1.1 Creo una variable bandera privada
	servicios: string[]; // 2.1 Creo Array público que almacenará los servicios ofrecidos por cada destino
  id= uuid();
  public votes = 0;
	constructor(public nombre:string, public imagenUrl: string ) {

		this.servicios =['pileta','desayuno']; // 2.2 inicializo el Array con valores por defecto.
	}
	isSelected(){ // 1.2creo método para identificar si está seleccionado o no
		return this.selected;
	}
	setSelected(s: boolean){   // 1.3creo otro método que se encarga de marcar cuando se seleccione
		// este método recibe un parámetro bool( y queda parametrizable).. modificación hecha *
	this.selected = s;
	}
	voteUp() : any{
		this.votes++;
	}
	voteDown() :any {
		this.votes--;
		
	}
}

import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject, from } from 'rxjs'; // programacion reactiva 
import { AppState, APP_CONFIG, AppConfig,db, MyDataBase} from '../app.module'; //acá importo la variable db
import {Store, INIT} from '@ngrx/store';
import { NuevoDestinoAction,
   ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';

import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse, HttpEvent} from '@angular/common/http'; // importación para comunicar proyectos?
//import { FormDestinoViajeComponent } from './../components/form-destino-viaje/form-destino-viaje.component';

@Injectable() 
export class DestinosApiClient {

  //creamos un objeto(current) tipo Subject(rxjs) y le setearemos un valor por defecto: new BehaviorSubject<>(null)
  // a este objeto se le publicarán las notificaciones acerca del nuevo destino que está siendo elegido como favorito

  destinos: DestinoViaje[]=[];
  constructor( 
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config : AppConfig, //forwardref para hacerle llamadas al web service
    private http: HttpClient
    ) {
    this.store
      .select(state => state.destinos)
      .subscribe((data)=> {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
     this.store
     .subscribe((data)=> {
       console.log('all store');
       console.log(data);
     });    
  }
  add(d:DestinoViaje){ //Crea un rquest donde se hace un POST(envia info a una URL)
    //aqui invocariamos al servidor 
    const headers: HttpHeaders = new HttpHeaders ({'X-API-TOKEN': 'token-seguridad'});//info modo diccionario clave-valor
    //informacion que se manda en el body(nuevo) y header
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my',{ nuevo: d.nombre}, { headers: headers});
    this.http.request(req).subscribe((data: HttpResponse<{}>) =>{
      if (data.status === 200) { // si la llamada al web service da 200(codigo de estatus de respuesta http)
        this.store.dispatch(new NuevoDestinoAction(d)); //despachamos la accion de destino
        const myDb =db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
      
  }

  getById(id: String): DestinoViaje {
    return this.destinos.filter(function(d){return d.id.toString() === id; })[0]; 
  }
  getAll(): DestinoViaje[] {
    return this.destinos;
  }


  elegir(d: DestinoViaje){ // metodo que sustituye el "elegidos" de otro codigo*
  this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
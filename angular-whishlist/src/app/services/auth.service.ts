import { Injectable } from '@angular/core'; //servicio inyectable para login

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user:string, password: string):boolean{
    if (user == 'user' && password == 'password'){ //si nos logueamos bien..
      localStorage.setItem('username',user);// Usamos LocalStorage, que es un componente de html5 que guarda estos valores en el navegador con el set.
      return true;
    }
    
    return false;

  }
  logout(): any {
    localStorage.removeItem('username');
  }

  getUser(): any{ //levanta el valor que se hizo en el set de arriba 
    return localStorage.getItem('username');

  }
  isLoggedIn(): boolean { //usa el get user(que por detrás usa el localStorage) si es distinto de null
    return this.getUser() !== null;
  }
}

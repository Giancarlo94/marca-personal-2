import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;
  
  constructor(private elRef: ElementRef) {
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
   }
   track(evento: Event): void {
     const elemenTags = this.element.attributes.getNamedItem('data-tracker-tags').value.split('');
     console.log('|||||||||| track evento: "${elemenTags}"'); //en la vida real, acá deberia ir la logica que guarde los likes en un servidor
   }

}

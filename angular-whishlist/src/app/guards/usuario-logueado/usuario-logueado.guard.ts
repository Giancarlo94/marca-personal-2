import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioLogueadoGuard implements CanActivate {
  constructor(private authService: AuthService){} //login
  //XanActivate analiza de acuerdo al estado de la ruta y al siguiente elemento
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>| Promise<boolean> | boolean {
    const isLoggedIn = this.authService.isLoggedIn(); // el guard se activa siempre y cuando AuthService dice que el 
    //..usuario está logueado **** 2. IR A AUTHSERVICE.TS***
    console.log ('canActivate', isLoggedIn);
    return isLoggedIn;
  }
  
}
